<?php



$regex = "/^[a-zA-Z0-9-+._]+@[a-zA-Z0-9-]{2,}\.[a-zA-Z]{2,}$/";
//? Si mon email ne correspond pas à l'ER alors
if (!preg_match($regex, $_POST["email"])) {
    //* J'affiche un message d'erreur
    echo "Email au mauvais format";
    //! J'arrête l'exécution du reste du script
    die;
}

$regex = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,12}$/";
//? Si mon mot de passe ne correspond pas à l'ER alors
if (!preg_match($regex, $_POST["pwd"])) {
    //* J'affiche un message d'erreur
    echo "Mot de passe au mauvais format";
    //! J'arrête l'exécution du reste du script
    die;
}
