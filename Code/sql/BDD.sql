CREATE TABLE users (
    id_users INT(11) AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    first_name VARCHAR(20),
    email VARCHAR(30) NOT NULL,
    password VARCHAR(30) NOT NULL
);

CREATE TABLE software (
    id_software INT(11) AUTO_INCREMENT PRIMARY KEY,
    last_name VARCHAR(30) NOT NULL,
    description TEXT,
    version VARCHAR(30) NOT NULL,
    video_link TEXT NOT NULL,
    software_link TEXT NOT NULL,
    id_category INT(11),
    CONSTRAINT fk_category FOREIGN KEY (id_category) REFERENCES category(id_category)
);

CREATE TABLE category (
    id_category INT(11) AUTO_INCREMENT PRIMARY KEY,
    last_name VARCHAR(30) NOT NULL
);

CREATE TABLE comment (
    id_comment INT(11) AUTO_INCREMENT PRIMARY KEY,
    comment TEXT,
    comment_date DATE,
    id_users INT(11),
    id_software INT(11),
    CONSTRAINT fk_users FOREIGN KEY (id_users) REFERENCES users(id_users),
    CONSTRAINT fk_software FOREIGN KEY (id_software) REFERENCES software(id_software)
);